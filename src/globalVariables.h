/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#pragma once
#include "pgr.h"
#include "setup.h"
#include "sceneGraph.h"

//constants
const int REFRESH_RATE = 16;
const float MAX_CAMERA_ELEVATION_ANGLE = 89.0f;
const float CAMERA_ROTATION_STEP = 0.15f;
const float MOVEMENT_SPEED = 0.05f;

//key mapping
const int KEYS_NUMBER = 8;
//WASD
const int W = 0;
const int S = 1;
const int A = 2;
const int D = 3;
//arrows
const int TOP_ARROW = 4;
const int BOT_ARROW = 5;
const int LEFT_ARROW = 6;
const int RIGHT_ARROW = 7;

//info about the current game state
extern bool keys[KEYS_NUMBER];
extern float currentMovementSpeed;
extern float gameTime;
extern int windowWidth;
extern int windowHeight;

//3D models
extern Tmodel* flooring;
extern Tmodel* wall;
extern Tmodel* panel;
extern Tmodel* window;
extern Tmodel* door;
extern Tmodel* skewed;
extern Tmodel* stair;
extern Tmodel* spaceship;
extern Tmodel* knife;
extern Tmodel* gun;
extern Tmodel* glass;
extern Tmodel* skybox;

//shaders
extern TsimpleShader simpleShader;
extern TskyboxShader skyboxShader;
extern TknifeShader knifeShader;
extern TglassShader glassShader;

//scene graph
extern CsceneGraph sceneGraph;


//picking
extern unsigned char pickID;
//knife variable color
extern float knifeOffset;
extern bool shine;
extern bool glassOffset;