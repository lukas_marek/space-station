/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#version 140

struct Material {
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float shininess;

  bool  useTexture;
};

uniform sampler2D texSampler;
uniform Material material;

smooth in vec4 vertexColor;
smooth in vec2 vertexTextureCoordinates;
in float visibility; //(fog)

out vec4 fragmentColor;

void main() {

  fragmentColor = vertexColor;

  fragmentColor =  vertexColor * texture(texSampler, vertexTextureCoordinates);

  //fog
  fragmentColor = mix(vec4(0.0,0.0,0.0,1.0), fragmentColor, visibility);
}
