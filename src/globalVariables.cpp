/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#include "globalVariables.h"

//info about the current game state
bool keys[KEYS_NUMBER] = { 0 };
float currentMovementSpeed = MOVEMENT_SPEED;
float gameTime = 0;
int windowWidth = 1920;
int windowHeight = 1080;

//info about the current loaded models
Tmodel* flooring;
Tmodel* wall;
Tmodel* panel;
Tmodel* window;
Tmodel* door;
Tmodel* skewed;
Tmodel* stair;
Tmodel* spaceship;
Tmodel* knife;
Tmodel* gun;
Tmodel* glass;
Tmodel* skybox;

//atributes locations
TsimpleShader simpleShader;
TskyboxShader skyboxShader;
TknifeShader knifeShader;
TglassShader glassShader;

//scene graph
CsceneGraph sceneGraph;

//picking
unsigned char pickID = 2;
//knife variable color
float knifeOffset = 0;
//turning of stair lights
bool shine = true;
//moves the glass force field
bool glassOffset = false;