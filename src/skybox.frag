/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#version 140

in vec3 vertexTextureCoordinates;

uniform samplerCube skyboxSampler;

out vec4 fragmentColor;

void main() 
{
  fragmentColor = texture(skyboxSampler, vertexTextureCoordinates);
}