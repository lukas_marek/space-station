/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#include <iostream>
#include "pgr.h"
#include "setup.h"
#include "callbacks.h"
#include "globalVariables.h"

using namespace std;


void setup(int argc, char* argv[]) {
	//setups screen and openGL
	screenSetup(argc, argv);

	//load shaders and link em to a single program
	loadShaders();

	//load objects with textures
	loadModels();

	//builds hierarchy of objects, shaders and transformations
	buildSceneGraph();
}

void callbacks() {
	//actual drawing
	glutDisplayFunc(onDisplay);
	//callback reshaping window
	glutReshapeFunc(onReshape);
	//callback catching ASCII keys
	glutKeyboardFunc(onKeyDown);
	glutKeyboardUpFunc(onKeyUp);
	//callback catching special keys
	glutSpecialFunc(onSpecialKeyPress);
	glutSpecialUpFunc(onSpecialKeyRelease);
	//callback catching mouse movement
	glutPassiveMotionFunc(onMouseMovement);
	//callback catching mouse clicks
	glutMouseFunc(onMouseClick);
	//time loop changing variables
	glutTimerFunc(REFRESH_RATE, onTimer, 0);
}

int main(int argc, char* argv[]) {
	
	setup(argc, argv);

	callbacks();

	cout << "....................................." << endl;
	cout << endl << "Press:" << endl << "   F1 for perspective view" << endl << "   F2 for front view" << endl << "   F3 for left view" << endl;

	//keeps the window opened and callbacks functional
	glutMainLoop();

	//cleanup
	cleanup();

	return 0;
}
