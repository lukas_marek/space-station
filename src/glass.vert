/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#version 140

struct Material {
  vec3  ambient; 
  vec3  diffuse; 
  vec3  specular;  
  float shininess; 

  bool  useTexture;
};

uniform Material material;

uniform sampler2D textureSampler;

uniform mat4 PVM;     
uniform mat4 V;       
uniform mat4 M;       
uniform mat4 N;  

uniform vec3 flashlightPosition; 
uniform vec3 flashlightDirection;

uniform bool shine;

in vec3 position;
in vec3 normal;  
in vec2 textureCoordinates;

smooth out vec2 vertexTextureCoordinates;
smooth out vec4 vertexColor;
out float visibility; //(fog)


struct Light {   
  vec3  ambient; 
  vec3  diffuse; 
  vec3  specular;
  vec3  position;
  //spotlight parametrs
  vec3  spotDirection;
  float spotCosCutOff;
  float spotExponent;
};

Light sun;
Light flashLight;
Light bulb0;
Light bulb1;

void lightsInitialization() {
  //sun moving with camera
  //sun.position = vec3(1.0, 5.0, -10.0);

  sun.ambient  = vec3(0.075);
  sun.diffuse  = vec3(0.6f, 0.5f, 0.5f);
  sun.specular = vec3(1.0);
  sun.position = (V * vec4(1.0, 5.0, -10.0, 1.0)).xyz;

  flashLight.ambient       = vec3(0.0f);
  flashLight.diffuse       = vec3(1.0);
  flashLight.specular      = vec3(1.0);
  flashLight.spotCosCutOff = 0.95f;
  flashLight.spotExponent  = 5.0;
  flashLight.position = (V * vec4(flashlightPosition, 1.0)).xyz;
  flashLight.spotDirection = normalize((V * vec4(flashlightDirection, 0.0)).xyz);

  //point lights (above stairs)
  bulb0.ambient       = vec3(0.0f);
  bulb0.diffuse       = vec3(255.0, 167.0, 81.0) / 150;
  bulb0.specular      = vec3(1.0);
  bulb0.position = (V * vec4(1.5, 2.1, 0.5, 1.0)).xyz;

  bulb1.ambient       = vec3(0.0f);
  bulb1.diffuse       = vec3(255.0, 167.0, 81.0) / 150;
  bulb1.specular      = vec3(1.0);
  bulb1.position = (V * vec4(1.5, 2.1, 0.0, 1.0)).xyz;
}

vec4 pointLight(Light light, Material material, vec3 vertexEyePosition, vec3 fixedNormal){
    
    vec3 catchedLight = vec3(0.0);
    
    //vector towards light source
    vec3 L = normalize(light.position - vertexEyePosition);
    vec3 R = reflect(-L, fixedNormal);
    vec3 V = normalize(-vertexEyePosition);

    float dist = length(light.position - vertexEyePosition);
    float attenuation = min(1.0f / (dist * dist), 1.0f);

    //max so its not negative
    float perpendicularLN = max(0.0, dot(fixedNormal, L));
    float perpendicularVR = max(0.0, dot(R, V));

    //add light
    catchedLight += material.diffuse * light.diffuse;
    catchedLight += material.diffuse * light.diffuse * perpendicularLN;
    catchedLight += material.specular * light.specular * pow(perpendicularVR, material.shininess);
    catchedLight *= max(dot(fixedNormal, L) / dist, 0.0f) * attenuation;

    return vec4(catchedLight, 1.0);
}

vec4 spotLight(Light light, Material material, vec3 vertexEyePosition, vec3 fixedNormal) {

  vec3 catchedLight = vec3(0.0);
  //vector towards light source (not only direction from origin but from hand)
  vec3 L = normalize(light.position - vertexEyePosition);
  //vector facing oposite side (reflected, really...)
  vec3 R = reflect(-L, fixedNormal);
  //"surface"
  vec3 V = normalize(-vertexEyePosition);

  //max so its not negative
  float perpendicularLN = max(0.0, dot(L, fixedNormal));
  float perpendicularVR = max(0.0, dot(V, R));

  catchedLight += material.ambient * light.ambient;
  catchedLight += material.diffuse * light.diffuse * perpendicularLN;
  catchedLight += material.specular * light.specular * pow(perpendicularVR, material.shininess);

  //crop out light outside of the cone
  float coeficient = max(0.0, dot(-L, light.spotDirection));
  if(coeficient < light.spotCosCutOff)
    catchedLight *= 0.0;
  else
    catchedLight *= pow(coeficient, light.spotExponent); //smooth out the transition

  return vec4(catchedLight, 1.0);
}

vec4 directionalLight(Light light, Material material, vec3 vertexEyePosition, vec3 fixedNormal) {

  vec3 catchedLight = vec3(0.0);
  //vector towards light source
  vec3 L = normalize(light.position);
  //vector facing oposite side (reflected, really...)
  vec3 R = reflect(-L, fixedNormal);
  //"surface"
  vec3 V = normalize(-vertexEyePosition);

  //max so its not negative
  float perpendicularLN = max(0.0, dot(L, fixedNormal));
  float perpendicularVR = max(0.0, dot(V, R));

  //add light
  catchedLight += material.ambient * light.ambient;
  catchedLight += material.diffuse * light.diffuse * perpendicularLN;
  catchedLight += material.specular * light.specular * pow(perpendicularVR, material.shininess);

  return vec4(catchedLight, 1.0);
}

float fog(vec3 vertexEyePosition){
  const float density = 0.25;
  const float gradient = 1.5;
  float dist = length(vertexEyePosition); //distance
  float vis = 0.0;//visibility

  //"hack if" -unobscures everything outside the station (do not apply fog to spaceship)
  if((M * vec4(position, 1.0)).z > -2.0)
    vis = exp(-pow((dist*density), gradient)); //out
  else
    vis = 1.0; //out

   return vis;
}

void main() {

  lightsInitialization();

  // eye-coordinates position and normal of vertex
  vec3 vertexEyePosition = (V * M * vec4(position, 1.0)).xyz;         // vertex in eye coordinates
  vec3 fixedNormal = normalize( (V * N * vec4(normal, 0.0) ).xyz);   // normal in eye coordinates by NormalMatrix

  vertexColor = vec4(0.0);

  // accumulate contributions from all lights
  vertexColor += directionalLight(sun, material, vertexEyePosition, fixedNormal);
  vertexColor += spotLight(flashLight, material, vertexEyePosition, fixedNormal) / 2.0f;
  if (shine == true){
    vertexColor += pointLight(bulb0, material, vertexEyePosition, fixedNormal);
    vertexColor += pointLight(bulb1, material, vertexEyePosition, fixedNormal);
  }

  //fog
  visibility = fog(vertexEyePosition);

  vertexTextureCoordinates = textureCoordinates; //out

  // vertex position after the projection (gl_Position is built-in output variable)
  gl_Position = PVM * vec4(position, 1);   // out:v vertex in clip coordinates
}
