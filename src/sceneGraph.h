/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#pragma once
#include "pgr.h"
#include "setup.h"
#include <map>
#include <string>
#include <vector>

using namespace std;

/** Main parent abstract node*/
class Cnode {
public:
	Cnode(Cnode* parent);
	virtual Cnode* up(vector<glm::mat4*>& collectedTransformations);
	virtual Cnode* down(vector<glm::mat4*>& collectedTransformations) = 0;
	virtual void update();
	vector<Cnode*> m_children;
	int m_processedChildrenNO;
	Cnode* m_parent;
	bool m_applied;
};

/** Node holding a transfromation matrix and lambda function which allows updating its transformation during runtime*/
class CtransformationNode : public Cnode {
public:
	CtransformationNode(Cnode* parent, glm::mat4(*transformationFunction)());
	Cnode* up(vector<glm::mat4*>& collectedTransformations) override;
	Cnode* down(vector<glm::mat4*>& collectedTransformations) override;
	void update() override;
private:
	glm::mat4(*m_transformationFunction)();
	glm::mat4 m_transformation;
};

/** Node setting a shader, material and a texture*/
class CmaterialNode : public Cnode {
public:
	CmaterialNode(Cnode* parent, Tmodel* model);
	Cnode* down(vector<glm::mat4*>& collectedTransformations) override;
private:
	void simpleShaderMaterial();
	void skyboxShaderMaterial();
	void knifeShaderMaterial();
	void glassShaderMaterial();
	const Tmodel* m_model;
};

/** Leaf node responsible for drawing a mesh*/
class CmeshNode : public Cnode {
public:
	CmeshNode(Cnode* parent, Tmodel* model);
	Cnode* down(vector<glm::mat4*>& collectedTransformations) override;
private:
	glm::mat4 calculateModelMatrix(vector<glm::mat4*>& collectedTransformations);
	void drawSimpleShaderMesh(vector<glm::mat4*>& collectedTransformations);
	void drawSkyboxShaderMesh();
	void drawKnifeShaderMesh(vector<glm::mat4*>& collectedTransformations);
	void drawGlassShaderMesh(vector<glm::mat4*>& collectedTransformations);
	const Tmodel* m_model;
};

/** Tree structure holding all nodes such as material node, transformation node and mesh node*/
class CsceneGraph {
public:
	CsceneGraph();
	/** Deallocates the whole thing*/
	~CsceneGraph();
	void addTransformationNode(Cnode* parent, string name, glm::mat4(*transformationFunction)(), bool animation);
	void addMaterialNode(Cnode* parent, string name, Tmodel* model);
	void addMeshNode(Cnode* parent, string name, Tmodel* model);
	/** Traverses the graph. This method is called every frame and is responsible for rendering the whole scene*/
	void traverseGraph();
	map<string, Cnode*> m_nodes;
private:
	vector<Cnode*> m_nodesToUpdate;
	/** Updates all transformation nodes which needs updating. For exmple their transformation is dependent on time, or some other variable.*/
	void updateNodes();
	vector<glm::mat4 *> m_collectedTransformations;
};