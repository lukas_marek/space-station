/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#include <iostream>
#include <string>
#include "pgr.h"
#include "setup.h"
#include "globalVariables.h"
#include "sceneGraph.h"
#include "camera.h"
#include "3D_models/gun/gun.h"

using namespace std;


//sets up screen and openGL
void screenSetup(int argc, char* argv[]) {
	//glut nutn� cajky
	glutInit(&argc, argv);
	glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

	//window properties
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
	
	//window
	glutCreateWindow("marekl11");
	glutInitWindowSize(windowWidth, windowHeight);
	glutFullScreen();
	glutSetCursor(GLUT_CURSOR_NONE);

	//pgr framework cajky
	pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);

	//sets background color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//backface culling
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	//alpha blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//loads shaders
void loadShaders() {
	vector<GLuint> shaderList;
		
	///////////////////////////////////////////////////////////////////////////////////////simple
	// push vertex shader and fragment shader
	shaderList.emplace_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "simple.vert"));
	shaderList.emplace_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "simple.frag"));

	// create the shader program with two shaders
	simpleShader.program = pgr::createProgram(shaderList);

	simpleShader.locationPosition = glGetAttribLocation(simpleShader.program, "position");
	simpleShader.locationNormal = glGetAttribLocation(simpleShader.program, "normal");
	simpleShader.locationTextureCoordinate = glGetAttribLocation(simpleShader.program, "textureCoordinates");
	// get uniforms locations
	simpleShader.locationPVM = glGetUniformLocation(simpleShader.program, "PVM");
	simpleShader.locationV = glGetUniformLocation(simpleShader.program, "V");
	simpleShader.locationM = glGetUniformLocation(simpleShader.program, "M");
	simpleShader.locationN = glGetUniformLocation(simpleShader.program, "N");
	// material
	simpleShader.locationAmbient = glGetUniformLocation(simpleShader.program, "material.ambient");
	simpleShader.locationDiffuse = glGetUniformLocation(simpleShader.program, "material.diffuse");
	simpleShader.locationSpecular = glGetUniformLocation(simpleShader.program, "material.specular");
	simpleShader.locationShinines = glGetUniformLocation(simpleShader.program, "material.shininess");
	// texture
	simpleShader.locationTextureSampler = glGetUniformLocation(simpleShader.program, "textureSampler");
	simpleShader.locationUseTexture = glGetUniformLocation(simpleShader.program, "material.useTexture");
	// flashlight
	simpleShader.locationFlashlightPosition = glGetUniformLocation(simpleShader.program, "flashlightPosition");
	simpleShader.locationFlashlightDirection = glGetUniformLocation(simpleShader.program, "flashlightDirection");
	//shine
	simpleShader.locationShine = glGetUniformLocation(simpleShader.program, "shine");
	
	shaderList.clear();

	///////////////////////////////////////////////////////////////////////////////////////skybox
	// push vertex shader and fragment shader
	shaderList.emplace_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "skybox.vert"));
	shaderList.emplace_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "skybox.frag"));

	// create the program with two shaders
	skyboxShader.program = pgr::createProgram(shaderList);

	// handles to vertex attributes locations
	skyboxShader.locationScreenCoordinations = glGetAttribLocation(skyboxShader.program, "screenCoordinations");
	// get uniforms locations
	skyboxShader.locationTextureSampler = glGetUniformLocation(skyboxShader.program, "skyboxSampler");
	skyboxShader.locationInversePV = glGetUniformLocation(skyboxShader.program, "inversePV");

	shaderList.clear();

	///////////////////////////////////////////////////////////////////////////////////////knife
	// push vertex shader and fragment shader
	shaderList.emplace_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "knife.vert"));
	shaderList.emplace_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "knife.frag"));

	// create the shader program with two shaders
	knifeShader.program = pgr::createProgram(shaderList);

	knifeShader.locationPosition = glGetAttribLocation(knifeShader.program, "position");
	knifeShader.locationNormal = glGetAttribLocation(knifeShader.program, "normal");
	knifeShader.locationTextureCoordinate = glGetAttribLocation(knifeShader.program, "textureCoordinates");
	// get uniforms locations
	knifeShader.locationPVM = glGetUniformLocation(knifeShader.program, "PVM");
	knifeShader.locationV = glGetUniformLocation(knifeShader.program, "V");
	knifeShader.locationM = glGetUniformLocation(knifeShader.program, "M");
	knifeShader.locationN = glGetUniformLocation(knifeShader.program, "N");
	// material
	knifeShader.locationAmbient = glGetUniformLocation(knifeShader.program, "material.ambient");
	knifeShader.locationDiffuse = glGetUniformLocation(knifeShader.program, "material.diffuse");
	knifeShader.locationSpecular = glGetUniformLocation(knifeShader.program, "material.specular");
	knifeShader.locationShinines = glGetUniformLocation(knifeShader.program, "material.shininess");
	// texture
	knifeShader.locationTextureSampler = glGetUniformLocation(knifeShader.program, "textureSampler");
	knifeShader.locationUseTexture = glGetUniformLocation(knifeShader.program, "material.useTexture");
	// flashlight
	knifeShader.locationFlashlightPosition = glGetUniformLocation(knifeShader.program, "flashlightPosition");
	knifeShader.locationFlashlightDirection = glGetUniformLocation(knifeShader.program, "flashlightDirection");
	//offset
	knifeShader.locationOffset = glGetUniformLocation(knifeShader.program, "offset");
	//shine
	knifeShader.locationShine = glGetUniformLocation(knifeShader.program, "shine");

	shaderList.clear();

	///////////////////////////////////////////////////////////////////////////////////////glass
	// push vertex shader and fragment shader
	shaderList.emplace_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "glass.vert"));
	shaderList.emplace_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "glass.frag"));

	// create the shader program with two shaders
	glassShader.program = pgr::createProgram(shaderList);

	glassShader.locationPosition = glGetAttribLocation(glassShader.program, "position");
	glassShader.locationNormal = glGetAttribLocation(glassShader.program, "normal");
	glassShader.locationTextureCoordinate = glGetAttribLocation(glassShader.program, "textureCoordinates");
	// get uniforms locations
	glassShader.locationPVM = glGetUniformLocation(glassShader.program, "PVM");
	glassShader.locationV = glGetUniformLocation(glassShader.program, "V");
	glassShader.locationM = glGetUniformLocation(glassShader.program, "M");
	glassShader.locationN = glGetUniformLocation(glassShader.program, "N");
	// material
	glassShader.locationAmbient = glGetUniformLocation(glassShader.program, "material.ambient");
	glassShader.locationDiffuse = glGetUniformLocation(glassShader.program, "material.diffuse");
	glassShader.locationSpecular = glGetUniformLocation(glassShader.program, "material.specular");
	glassShader.locationShinines = glGetUniformLocation(glassShader.program, "material.shininess");
	// texture
	glassShader.locationTextureSampler = glGetUniformLocation(glassShader.program, "textureSampler");
	glassShader.locationUseTexture = glGetUniformLocation(glassShader.program, "material.useTexture");
	// flashlight
	glassShader.locationFlashlightPosition = glGetUniformLocation(glassShader.program, "flashlightPosition");
	glassShader.locationFlashlightDirection = glGetUniformLocation(glassShader.program, "flashlightDirection");
	//offset
	glassShader.locationOffset = glGetUniformLocation(glassShader.program, "offset");
	//shine
	glassShader.locationShine = glGetUniformLocation(glassShader.program, "shine");
}

//loads objects
void loadModels() 
{
	//skybox initialization
	initSkyboxGeometry(&skybox, skyboxShader.ID);
	//flooring
	loadModel("3D_models/flooring/flooring.obj", &flooring, simpleShader.ID);
	//wall
	loadModel("3D_models/wall/wall.obj", &wall, simpleShader.ID);
	//panel
	loadModel("3D_models/panel/panel.obj", &panel, simpleShader.ID);
	//panel
	loadModel("3D_models/window/window.obj", &window, simpleShader.ID);
	//door
	loadModel("3D_models/door/door.obj", &door, simpleShader.ID);
	//skewed
	loadModel("3D_models/skewed/skewed.obj", &skewed, simpleShader.ID);
	//stair
	loadModel("3D_models/stair/stair.obj", &stair, simpleShader.ID);
	//spaceship
	loadModel("3D_models/spaceship/spaceship.obj", &spaceship, simpleShader.ID);
	//spaceship
	loadModel("3D_models/knife/knife.obj", &knife, knifeShader.ID);
	//glass
	loadModel("3D_models/glass/glass.obj", &glass, glassShader.ID);
	//gun
	loadMeshFromArray(&gun, "3D_models/gun/gun_LowPoly_Albedo.png");
}

//load some 3D format into openGL buffers
bool loadModel(const string &fileName, Tmodel **geometry, const int shaderID) {
	Assimp::Importer importer;

	// Load asset from the file - you can play with various processing steps
	const aiScene* scn = importer.ReadFile(fileName.c_str(), 0 | aiProcess_Triangulate);

	// abort if the loader fails
	if (scn == NULL) {
		std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
		*geometry = NULL;
		return false;
	}

	// some formats store whole scene (multiple meshes and materials, lights, cameras, ...) in one file, we cannot handle that in our simplified example
	if (scn->mNumMeshes != 1) {
		std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
		*geometry = NULL;
		return false;
	}

	// in this phase we know we have one mesh in our loaded scene, we can directly copy its data to OpenGL ...
	const aiMesh* mesh = scn->mMeshes[0];

	*geometry = new Tmodel;

	// vertex buffer object, store all vertex positions and normals
	glGenBuffers(1, &((*geometry)->vbo));
	glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vbo);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float) * mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
	// first store all vertices
	glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(float) * mesh->mNumVertices, mesh->mVertices);
	// then store all normals
	glBufferSubData(GL_ARRAY_BUFFER, 3 * sizeof(float) * mesh->mNumVertices, 3 * sizeof(float) * mesh->mNumVertices, mesh->mNormals);

	// just texture 0 for now
	float* textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
	float* currentTextureCoord = textureCoords;

	// copy texture coordinates
	aiVector3D vect;

	if (mesh->HasTextureCoords(0) == true) {
		// we use 2D textures with 2 coordinates and ignore the third coordinate
		for (unsigned int idx = 0; idx < mesh->mNumVertices; idx++) {
			vect = (mesh->mTextureCoords[0])[idx];
			*currentTextureCoord++ = vect.x;
			*currentTextureCoord++ = vect.y;
		}
	}

	// finally store all texture coordinates
	glBufferSubData(GL_ARRAY_BUFFER, 6 * sizeof(float) * mesh->mNumVertices, 2 * sizeof(float) * mesh->mNumVertices, textureCoords);

	// copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
	unsigned int* indices = new unsigned int[mesh->mNumFaces * 3];
	for (unsigned int f = 0; f < mesh->mNumFaces; ++f) {
		indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
		indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
		indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
	}

	// copy our temporary index array to OpenGL and free the array
	glGenBuffers(1, &((*geometry)->ebo));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

	delete[] indices;

	// copy the material info to Tmodel structure
	const aiMaterial* mat = scn->mMaterials[mesh->mMaterialIndex];
	aiColor4D color;
	aiString name;
	aiReturn retValue = AI_SUCCESS;

	// Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
	mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

	if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
		color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);

	(*geometry)->diffuse = glm::vec3(color.r, color.g, color.b);

	if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS)
		color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
	(*geometry)->ambient = glm::vec3(color.r, color.g, color.b);

	if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
		color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
	(*geometry)->specular = glm::vec3(color.r, color.g, color.b);

	ai_real shininess, strength;
	unsigned int max;	// changed: to unsigned

	max = 1;
	if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
		shininess = 1.0f;
	max = 1;
	if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
		strength = 1.0f;
	(*geometry)->shininess = shininess * strength;

	(*geometry)->texture = 0;

	// load texture image
	if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
		// get texture name 
		aiString path; // filename

		aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
		string textureName = path.data;

		size_t found = fileName.find_last_of("/\\");
		// insert correct texture file path 
		if (found != string::npos) { // not found
		  //subMesh_p->textureName.insert(0, "/");
			textureName.insert(0, fileName.substr(0, found + 1));
		}

		cout << "Loading texture file: " << textureName << endl;
		(*geometry)->texture = pgr::createTexture(textureName);
	}

	//vao
	glGenVertexArrays(1, &((*geometry)->vao));
	glBindVertexArray((*geometry)->vao);
	//ebo
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->ebo); // bind our element array buffer (indices) to vao
	//vbo
	glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vbo);

	if (shaderID == simpleShader.ID) {
		//position
		glEnableVertexAttribArray(simpleShader.locationPosition);
		glVertexAttribPointer(simpleShader.locationPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

		//lighting
		glEnableVertexAttribArray(simpleShader.locationNormal);
		glVertexAttribPointer(simpleShader.locationNormal, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));

		//textures
		glEnableVertexAttribArray(simpleShader.locationTextureCoordinate);
		glVertexAttribPointer(simpleShader.locationTextureCoordinate, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
	
	}
	else if (shaderID == knifeShader.ID) {
		//position
		glEnableVertexAttribArray(knifeShader.locationPosition);
		glVertexAttribPointer(knifeShader.locationPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

		//lighting
		glEnableVertexAttribArray(knifeShader.locationNormal);
		glVertexAttribPointer(knifeShader.locationNormal, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));

		//textures
		glEnableVertexAttribArray(knifeShader.locationTextureCoordinate);
		glVertexAttribPointer(knifeShader.locationTextureCoordinate, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
	}
	else if (shaderID == glassShader.ID) {
		//position
		glEnableVertexAttribArray(glassShader.locationPosition);
		glVertexAttribPointer(glassShader.locationPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

		//lighting
		glEnableVertexAttribArray(glassShader.locationNormal);
		glVertexAttribPointer(glassShader.locationNormal, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));

		//textures
		glEnableVertexAttribArray(glassShader.locationTextureCoordinate);
		glVertexAttribPointer(glassShader.locationTextureCoordinate, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
	}

	(*geometry)->shaderID = shaderID;
	
	glBindVertexArray(0);

	(*geometry)->trianglesNO = mesh->mNumFaces;

	return true;
}

//load object from C like array
void loadMeshFromArray(Tmodel** model, const string &textureName)
{
	//alocate new Tmodel
	*model = new Tmodel;

	//create vao
	glGenVertexArrays(1, &((*model)->vao));
	glBindVertexArray((*model)->vao);

	//create vbo
	glGenBuffers(1, &((*model)->vbo));
	glBindBuffer(GL_ARRAY_BUFFER, (*model)->vbo);
	//allocate memory for vertices, normals, and texture coordinates
	glBufferData(GL_ARRAY_BUFFER, sizeof(gunVertices), gunVertices, GL_STATIC_DRAW);

	//create ebo
	glGenBuffers(1, &((*model)->ebo));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*model)->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(gunTriangles), gunTriangles, GL_STATIC_DRAW);

	//positions
	glEnableVertexAttribArray(simpleShader.locationPosition);
	glVertexAttribPointer(simpleShader.locationPosition, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(0));
	//normals
	glEnableVertexAttribArray(simpleShader.locationNormal);
	glVertexAttribPointer(simpleShader.locationNormal, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	//UV
	glEnableVertexAttribArray(simpleShader.locationTextureCoordinate);
	glVertexAttribPointer(simpleShader.locationTextureCoordinate, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

	//load texture
	cout << "Loading texture file: " << textureName << endl;
	(*model)->texture = pgr::createTexture(textureName);

	(*model)->shaderID = 0;
	(*model)->trianglesNO = gunNTriangles;
}

//setup skybox mesh
void initSkyboxGeometry(Tmodel** geometry, const int shaderID) {

	*geometry = new Tmodel;

	// 2D coordinates of 2 triangles covering the whole screen (NDC), draw using triangle strip
	static const float screenCoords[] = {
	  -1.0f, -1.0f,
	   1.0f, -1.0f,
	  -1.0f,  1.0f,
	   1.0f,  1.0f
	};

	glGenVertexArrays(1, &((*geometry)->vao));
	glBindVertexArray((*geometry)->vao);

	// buffer for far plane rendering
	glGenBuffers(1, &((*geometry)->vbo));
	glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(screenCoords), screenCoords, GL_STATIC_DRAW);

	glEnableVertexAttribArray(skyboxShader.locationScreenCoordinations);
	glVertexAttribPointer(skyboxShader.locationScreenCoordinations, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	glUseProgram(0);
	CHECK_GL_ERROR();

	(*geometry)->trianglesNO = 2;

	glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &((*geometry)->texture));
	glBindTexture(GL_TEXTURE_CUBE_MAP, (*geometry)->texture);

	const char* suffixes[] = { "right", "left", "top", "bot", "front", "back" };
	GLuint targets[] = {
	  GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	  GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
	  GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};

	for (int i = 0; i < 6; i++) {
		string texName = string("3D_models/skybox/") + suffixes[i] + ".png";
		cout << "Loading cube map texture: " << texName << endl;
		pgr::loadTexImage2D(texName, targets[i]);
	}

	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	// unbind the texture (just in case someone will mess up with texture calls later)
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	(*geometry)->shaderID = shaderID;
}

//builds scene graph
void buildSceneGraph()
{
	cout << "Building scene graph" << endl;

	//skybox
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "skyboxMaterial", skybox);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["skyboxMaterial"], "skybox", skybox);

	//floorings
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "flooringMaterial", flooring);
	//flooring0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["flooringMaterial"], "staleFlooring0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-1.0f, 0.0f, 0.0f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleFlooring0"], "flooring0", flooring);
	//flooring1
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["flooringMaterial"], "rotatedFlooring1", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotatedFlooring1"], "staleFlooring1", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.75f, 0.0f, -0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleFlooring1"], "flooring1", flooring);

	//180 rotated floorings as ceiling
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["flooringMaterial"], "rotated180Floorings", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)); }, false);
	//flooring2
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180Floorings"], "rotatedFlooring2", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotatedFlooring2"], "staleFlooring2", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.75f, 2.5f, -0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleFlooring2"], "flooring2", flooring);
	//flooring3
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180Floorings"], "staleFlooring3", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.5f, 0.0f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleFlooring3"], "flooring3", flooring);

	//walls
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "wallMaterial", wall);
	//left walls
	//wall0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["wallMaterial"], "staleWall0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-1.5f, 0.0f, -0.75f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleWall0"], "wall0", wall);
	//wall1
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["wallMaterial"], "staleWall1", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-0.5f, 0.0f, -0.75f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleWall1"], "wall1", wall);
	//right walls
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["wallMaterial"], "rotated180walls", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	//wall2
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180walls"], "staleWall2", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-1.5f, 0.0f, 0.75f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleWall2"], "wall2", wall);
	//wall3
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180walls"], "staleWall3", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-0.5f, 0.0f, 0.75f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleWall3"], "wall3", wall);
	//wall4
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180walls"], "staleWall4", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.5f, 0.0f, 0.75f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleWall4"], "wall4", wall);

	//panels
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "panelMaterial", panel);
	//doors oriented panels
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["panelMaterial"], "doorOrientedPanels", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	//panel0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["doorOrientedPanels"], "stalePanel0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -1.0f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["stalePanel0"], "panel0", panel);
	//panel1
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["doorOrientedPanels"], "stalePanel1", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-2.0f, 0.0f, -0.5f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["stalePanel1"], "panel1", panel);
	//door facing
	//panel2
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["panelMaterial"], "stalePanel2", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(1.5f, 0.0f, -0.5f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["stalePanel2"], "panel2", panel);
	//panel3
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["panelMaterial"], "stalePanel3", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(1.5f, 0.0f, -1.0f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["stalePanel3"], "panel3", panel);
	//near stairs
	//panel4
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["panelMaterial"], "rotatedPanel4", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotatedPanel4"], "stalePanel4", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(1.25f, 0.0f, 0.75f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["stalePanel4"], "panel4", panel);

	//windows
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "windowMaterial", window);
	//window0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["windowMaterial"], "staleWindow0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.75f, 0.0f, -1.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleWindow0"], "window0", window);

	//doors
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "doorMaterial", door);
	//door0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["doorMaterial"], "staleDoor0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-2.0f, 0.0f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleDoor0"], "door0", door);
	//door1
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["doorMaterial"], "rotated180Door", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180Door"], "staleDoor1", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(5.5f, 2.0f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleDoor1"], "door1", door);

	//skeweds
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "skewedMaterial", skewed);
	//skewed0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["skewedMaterial"], "staleSkewed0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(2.5f, 1.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleSkewed0"], "skewed0", skewed);
	//skewed1
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["skewedMaterial"], "staleSkewed1", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(4.5f, 2.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleSkewed1"], "skewed1", skewed);

	//stairs
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "stairMaterial", stair);
	//stair0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(1.75f, 0.0f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair0"], "stair0", stair);
	//stair1
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair1", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(2.25f, 0.25f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair1"], "stair1", stair);
	//stair2
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair2", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(2.75f, 0.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair2"], "stair2", stair);
	//stair3
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair3", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(3.25f, 0.75f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair3"], "stair3", stair);
	//stair4
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair4", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(3.75f, 1.0f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair4"], "stair4", stair);
	//stair5
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair5", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(4.25f, 1.25f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair5"], "stair5", stair);
	//stair6
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair6", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(4.75f, 1.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair6"], "stair6", stair);
	//stair7
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "staleStair7", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(5.25f, 1.75f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair7"], "stair7", stair);
	//top stairs
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["stairMaterial"], "rotated180ZStair", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)); }, false);
	//stair8
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair8", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(5.25f, 4.0f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair8"], "stair8", stair);
	//stair9
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair9", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(4.75f, 3.75f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair9"], "stair9", stair);
	//stair10
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair10", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(4.25f, 3.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair10"], "stair10", stair);
	//stair11
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair11", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(3.75f, 3.25f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair11"], "stair11", stair);
	//stair12
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair12", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(3.25f, 3.0f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair12"], "stair12", stair);
	//stair13
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair13", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(2.75f, 2.75f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair13"], "stair13", stair);
	//stair14
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair14", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(2.25f, 2.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair14"], "stair14", stair);
	//stair15
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair15", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(1.75f, 2.25f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair15"], "stair15", stair);
	//above main door
	//stair16
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair16", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-1.75f, 2.25f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair16"], "stair16", stair);
	//stair17
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "staleStair17", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(-1.25f, 2.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair17"], "stair17", stair);
	//rotated above stairs
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180ZStair"], "rotated180YStair", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	//stair18
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotated180YStair"], "staleStair18", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(1.25f, 2.5f, 0.25f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleStair18"], "stair18", stair);

	//spaceship
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "spaceshipMaterial", spaceship);
	//spaceship0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["spaceshipMaterial"], "scaleSpaceship0", []() -> glm::mat4 {return glm::scale(glm::mat4(1.0f), glm::vec3(0.1f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["scaleSpaceship0"], "sklon", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(-110.0f), glm::vec3(1.0f, 0.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["sklon"], "odstup", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 10.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["odstup"], "let", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(gameTime * 20), glm::vec3(0.0f, 1.0f, 0.0f)); }, true);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["let"], "posunuti", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -30.0f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["posunuti"], "spaceship0", spaceship);

	//knife
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "knifeMaterial", knife);
	//knife0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["knifeMaterial"], "scaleKnife0", []() -> glm::mat4 {return glm::scale(glm::mat4(1.0f), glm::vec3(0.05f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["scaleKnife0"], "rotateUpKnife0", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(30.0f), glm::vec3(0.0f, 0.0f, 1.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotateUpKnife0"], "rotateSideKnife0", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(10.0f), glm::vec3(0.0f, 1.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotateSideKnife0"], "staleKnife0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.125f, 1.5f, -1.03f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleKnife0"], "knife0", knife);

	//gun
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "gunMaterial", gun);
	//gun0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["gunMaterial"], "scaleGun0", []() -> glm::mat4 {return glm::scale(glm::mat4(1.0f), glm::vec3(0.042f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["scaleGun0"], "rotategun0", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(70.0f), glm::vec3(1.0f, 0.0f, 0.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotategun0"], "rotate10gun0", []() -> glm::mat4 {return glm::rotate(glm::mat4(), glm::radians(10.0f), glm::vec3(0.0f, 0.0f, 1.0f)); }, false);
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["rotate10gun0"], "staleGun0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.13f, 0.37f, -1.7f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleGun0"], "gun0", gun);
	
	//glass
	sceneGraph.addMaterialNode(sceneGraph.m_nodes["root"], "glassMaterial", glass);
	//glass0
	sceneGraph.addTransformationNode(sceneGraph.m_nodes["glassMaterial"], "staleGlass0", []() -> glm::mat4 {return glm::translate(glm::mat4(), glm::vec3(0.75f, 1.25f, -1.45f)); }, false);
	sceneGraph.addMeshNode(sceneGraph.m_nodes["staleGlass0"], "glass0", glass);
}

//deletes VAO+VBO+EBO+texture of an object
void deleteBuffers(Tmodel* geometry) {
	//delete buffers
	glDeleteVertexArrays(1, &(geometry->vao));
	glDeleteBuffers(1, &(geometry->ebo));
	glDeleteBuffers(1, &(geometry->vbo));

	//delete texture
	if (geometry->texture != 0)
		glDeleteTextures(1, &(geometry->texture));
}

//cleanup
void cleanup() {
	cout << "....................................." << endl;
	cout << "Cleaning up" << endl;

	//delete objects
	deleteBuffers(flooring);
	deleteBuffers(wall);
	deleteBuffers(panel);
	deleteBuffers(window);
	deleteBuffers(door);
	deleteBuffers(skewed);
	deleteBuffers(stair);
	deleteBuffers(spaceship);
	deleteBuffers(knife);
	deleteBuffers(glass);
	deleteBuffers(skybox);

	//delete shaders
	pgr::deleteProgramAndShaders(simpleShader.program);
	pgr::deleteProgramAndShaders(skyboxShader.program);
}
