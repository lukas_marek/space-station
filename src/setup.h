/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#pragma once
#include <iostream>
#include <string>
#include "pgr.h"

using namespace std;

//info about the current loaded model
struct Tmodel {
	unsigned short int shaderID;
	GLuint        vbo;
	GLuint        ebo;
	GLuint        vao;  
	unsigned int  trianglesNO;       
	glm::vec3     ambient = glm::vec3(1.0f);
	glm::vec3     diffuse = glm::vec3(1.0f);
	glm::vec3     specular = glm::vec3(0.3f);
	float         shininess = 0.3f;
	GLuint        texture;
};

//locations of attributes
struct TsimpleShader {
	unsigned short int ID = 0;
	GLuint program;
	GLint locationPosition;
	GLint locationNormal;
	GLint locationTextureCoordinate;
	GLint locationPVM;
	GLint locationV;
	GLint locationM;     
	GLint locationN;
	GLint locationDiffuse;
	GLint locationAmbient;
	GLint locationSpecular;
	GLint locationShinines;
	GLint locationUseTexture;
	GLint locationTextureSampler;
	GLint locationFlashlightPosition;
	GLint locationFlashlightDirection;
	GLint locationShine;
};

struct TskyboxShader {
	unsigned short int ID = 1;
	GLuint program;                
	GLint locationScreenCoordinations;
	GLint locationInversePV;
	GLint locationTextureSampler;
};

struct TknifeShader {
	unsigned short int ID = 2;
	GLuint program;
	GLint locationPosition;
	GLint locationNormal;
	GLint locationTextureCoordinate;
	GLint locationPVM;
	GLint locationV;
	GLint locationM;
	GLint locationN;
	GLint locationDiffuse;
	GLint locationAmbient;
	GLint locationSpecular;
	GLint locationShinines;
	GLint locationUseTexture;
	GLint locationTextureSampler;
	GLint locationFlashlightPosition;
	GLint locationFlashlightDirection;
	GLint locationOffset;
	GLint locationShine;
};

struct TglassShader {
	unsigned short int ID = 3;
	GLuint program;
	GLint locationPosition;
	GLint locationNormal;
	GLint locationTextureCoordinate;
	GLint locationPVM;
	GLint locationV;
	GLint locationM;
	GLint locationN;
	GLint locationDiffuse;
	GLint locationAmbient;
	GLint locationSpecular;
	GLint locationShinines;
	GLint locationUseTexture;
	GLint locationTextureSampler;
	GLint locationFlashlightPosition;
	GLint locationFlashlightDirection;
	GLint locationOffset;
	GLint locationShine;
};

//sets up screen and openGL
void screenSetup(int argc, char* argv[]);

//loads shaders
void loadShaders();

//loads objects
void loadModels();

//load some 3D format into openGL buffers
bool loadModel(const std::string& fileName, Tmodel** geometry, const int shaderID);

//load object from C like array
void loadMeshFromArray(Tmodel** object, const string& textureName);

//setup skybox mesh
void initSkyboxGeometry(Tmodel** geometry, const int shaderID);

//builds scene graph
void buildSceneGraph();

//deletes VAO+VBO+EBO+texture of an object
void deleteBuffers(Tmodel* geometry);

//cleanup of buffers
void cleanup();