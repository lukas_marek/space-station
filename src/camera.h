/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#pragma once

struct Tcamera {
	glm::vec3 eye = glm::vec3(-1.5f, 1.75f, 0.0f);
	glm::vec3 defaultCenterVector = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 currentCenterVector;
	glm::vec3 upVector = glm::vec3(0.0f, 1.0f, 0.0f);
	float elevationAngle = 0.0f;
	float viewAngle = 0.0f;
};
extern Tcamera camera;
extern glm::mat4* PV;
extern glm::mat4 M;

//camera projection + rotation
glm::mat4* createPV();

//move camera in forward direction
void moveCameraFront();

//move camera backwards
void moveCameraBack();

//move camera to the right
void moveCameraLeft();

//move camera to the right
void moveCameraRight();

//check collision
bool checkCollision(const glm::vec3& destination);