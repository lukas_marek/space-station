/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#include <iostream>
#include "pgr.h"
#include "setup.h"
#include "camera.h"
#include "callbacks.h"
#include "math.h"
#include "globalVariables.h"

using namespace std;

//stop drawing while cleaning
static bool ende = false;

//draw function
void onDisplay() {
	//erases screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	//create projection and view matrix
	PV = createPV();

	if (ende == false)
		sceneGraph.traverseGraph();

	glutSwapBuffers();
}

//reshapes window
void onReshape(int width, int height) {
	glViewport(0, 0, width, height);
}

//keyboard down ASCII input
void onKeyDown(unsigned char key, int, int) {
	switch (key) {
	case 27:
		ende = true;
		glutLeaveMainLoop();
		cleanup();
		break;
	//movement
	case 'd':
		keys[D] = true;
		if (keys[W] == true || keys[S] == true) {
			currentMovementSpeed = MOVEMENT_SPEED * float(1 / sqrt(2));
		}
		break;
	case 'a':
		keys[A] = true;
		if (keys[W] == true || keys[S] == true) {
			currentMovementSpeed = MOVEMENT_SPEED * float(1 / sqrt(2));
		}
		break;
	case 'w':
		keys[W] = true;
		if (keys[D] == true || keys[A] == true) {
			currentMovementSpeed = MOVEMENT_SPEED * float(1 / sqrt(2));
		}
		break;
	case 's':
		keys[S] = true;
		if (keys[D] == true || keys[A] == true) {
			currentMovementSpeed = MOVEMENT_SPEED * float(1 / sqrt(2));
		}
		break;
	}
}

//keyboard release ASCII input
void onKeyUp(unsigned char key, int, int) {
	switch (key) {
	case 'd':
		keys[D] = false;
		currentMovementSpeed = MOVEMENT_SPEED;
		break;
	case 'a':
		keys[A] = false;
		currentMovementSpeed = MOVEMENT_SPEED;
		break;
	case 'w':
		keys[W] = false;
		currentMovementSpeed = MOVEMENT_SPEED;
		break;
	case 's':
		keys[S] = false;
		currentMovementSpeed = MOVEMENT_SPEED;
		break;
	}
}

//keyboard special keys input (press)
void onSpecialKeyPress(int specKeyPressed, int mouseX, int mouseY) {
	switch (specKeyPressed) {
	case GLUT_KEY_RIGHT:
		keys[RIGHT_ARROW] = true;
		break;
	case GLUT_KEY_LEFT:
		keys[LEFT_ARROW] = true;
		break;
	case GLUT_KEY_UP:
		keys[TOP_ARROW] = true;
		break;
	case GLUT_KEY_DOWN:
		keys[BOT_ARROW] = true;
		break;
	//views
	case GLUT_KEY_F1:
		camera.eye = glm::vec3(-1.5f, 1.75f, 0.0f);
		camera.defaultCenterVector = glm::vec3(1.0f, 0.0f, 0.0f);
		camera.elevationAngle = 0.0f;
		camera.viewAngle = 0.0f;
		cout << " Perspective view " << endl;
		break;
	case GLUT_KEY_F2:
		camera.eye = glm::vec3(1.0f, 1.75f, 0.0f);
		camera.defaultCenterVector = glm::vec3(-1.0f, 0.0f, 0.0f);
		camera.elevationAngle = 0.0f;
		camera.viewAngle = 0.0f;
		cout << " Front view " << endl;
		break;
	case GLUT_KEY_F3:
		camera.eye = glm::vec3(0.75f, 1.75f, 0.25f);
		camera.defaultCenterVector = glm::vec3(0.0f, 0.0f, -1.0f);
		camera.elevationAngle = 0.0f;
		camera.viewAngle = 0.0f;
		cout << " Left view " << endl;
		break;
	}
}

//keyboard special keys input (release)
void onSpecialKeyRelease(int specKeyReleased, int mouseX, int mouseY) {
	switch (specKeyReleased) {
	case GLUT_KEY_RIGHT:
		keys[RIGHT_ARROW] = false;
		break;
	case GLUT_KEY_LEFT:
		keys[LEFT_ARROW] = false;
		break;
	case GLUT_KEY_UP:
		keys[TOP_ARROW] = false;
		break;
	case GLUT_KEY_DOWN:
		keys[BOT_ARROW] = false;
		break;
	}
}

//mouse input (movement)
void onMouseMovement(int mouseX, int mouseY) {
	float cameraElevationAngleDelta = (CAMERA_ROTATION_STEP * (mouseY - glutGet(GLUT_WINDOW_HEIGHT) / 2));
	if (fabs(camera.elevationAngle + cameraElevationAngleDelta) < MAX_CAMERA_ELEVATION_ANGLE) {
		camera.elevationAngle += cameraElevationAngleDelta;
	}
	//horizontal movement
	camera.viewAngle -= (CAMERA_ROTATION_STEP * (mouseX - glutGet(GLUT_WINDOW_WIDTH) / 2));

	//reset pointer
	glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);
}

//mouse input (click)
void onMouseClick(int button, int state, int x, int y) {
	if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN)) {

		unsigned char ID = 0;
		glReadPixels(x, glutGet(GLUT_WINDOW_HEIGHT) - y, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &ID);

		switch (ID) {
		case 1:
			cout << "knife" << endl;
			++knifeOffset;
			break;

		case 16:
			cout << "window" << endl;
			if (glassOffset == false)
				glassOffset = true;
			else
				glassOffset = false;
			break;

		case 36:
			cout << "lights" << endl;
			if (shine == false)
				shine = true;
			else
				shine = false;
			break;
		}
	}
}

//time changing loop
void onTimer(int) {
	glutPostRedisplay();

	int timeMs = glutGet(GLUT_ELAPSED_TIME);
	gameTime = timeMs * 0.001f;

	//WASD
	if (keys[W] == true)
		moveCameraFront();
	if (keys[S] == true)
		moveCameraBack();
	if (keys[D] == true)
		moveCameraRight();
	if (keys[A] == true)
		moveCameraLeft();
	//arrows
	if (keys[TOP_ARROW] == true)
		moveCameraFront();
	if (keys[BOT_ARROW] == true)
		moveCameraBack();
	if (keys[RIGHT_ARROW] == true)
		moveCameraRight();
	if (keys[LEFT_ARROW] == true)
		moveCameraLeft();

	glutTimerFunc(REFRESH_RATE, onTimer, 0);
}

