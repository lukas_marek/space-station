/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#include "pgr.h"
#include "camera.h"
#include "globalVariables.h"

//camera
Tcamera camera;
glm::mat4* PV;
glm::mat4 M;

//camera projection + rotation
glm::mat4* createPV() {

    //create rotation matrix locked around upVector
    glm::mat4 sidesRotationMatrix = glm::rotate(glm::mat4(1.0f), glm::radians(camera.viewAngle), camera.upVector);
    //create new transformed center by rotating original camera center like vector around world origin and then adding it to camera eye
    glm::vec3 centerVector = glm::vec3(sidesRotationMatrix * glm::vec4(camera.defaultCenterVector, 0.0f));

    //create rotation matrix locked around horizont
    glm::mat4 elevationRotationMatrix = glm::rotate(glm::mat4(1.0f), glm::radians(camera.elevationAngle), glm::cross(camera.upVector, centerVector));
    //rotate camera center like vector around world origin and then add it to camera eye
    camera.currentCenterVector = glm::vec3(elevationRotationMatrix * glm::vec4(centerVector, 0.0f));
    
    //container of all "camera" matrices
    static glm::mat4 PV[2];
    //projection
    PV[0] = glm::perspective(glm::radians(60.0f), glutGet(GLUT_WINDOW_WIDTH) / (float)glutGet(GLUT_WINDOW_HEIGHT), 0.01f, 100.0f);
    //view
    PV[1] = glm::lookAt(camera.eye, camera.currentCenterVector + camera.eye, camera.upVector);
    
    return PV;
}

void moveCameraFront() {
    glm::vec3 preview = camera.eye;
    preview += currentMovementSpeed * normalize(glm::vec3(camera.currentCenterVector.x, 0.0f, camera.currentCenterVector.z));
    if (checkCollision(preview) == false)
        camera.eye = preview;
}

void moveCameraBack() {
    glm::vec3 preview = camera.eye;
    preview -= currentMovementSpeed * normalize(glm::vec3(camera.currentCenterVector.x, 0.0f, camera.currentCenterVector.z));
    if (checkCollision(preview) == false)
        camera.eye = preview;
}

void moveCameraLeft() {
    glm::vec3 preview = camera.eye;
    preview -= currentMovementSpeed * normalize(cross(camera.currentCenterVector, camera.upVector));
    if (checkCollision(preview) == false)
        camera.eye = preview;
}

void moveCameraRight() {
    glm::vec3 preview = camera.eye;
    preview += currentMovementSpeed * normalize(cross(camera.currentCenterVector, camera.upVector));
    if (checkCollision(preview) == false)
        camera.eye = preview;
}

bool checkCollision(const glm::vec3& destination) {
    if ((destination.x > -1.75f && destination.x < 1.25f && destination.z > -0.5f && destination.z < 0.5f) || ((destination.x > 0.25f && destination.x < 1.25f && destination.z > -1.25f && destination.z < 0.5f)))
    {
        return false;
    }

    return true;
}