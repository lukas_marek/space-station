/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#pragma once

//help function to draw specified VAO in onDisplay() function
void drawSimpleShaderMesh(Tmodel &object, TsimpleShader &shader,  glm::mat4 &P,  glm::mat4 &V, glm::mat4 &M);

//draw function
void onDisplay();

//reshapes window
void onReshape(int width, int height);

//keyboard down ASCII input
void onKeyDown(unsigned char key, int, int);

//keyboard release ASCII input
void onKeyUp(unsigned char key, int, int);

//keyboard special keys input (press)
void onSpecialKeyPress(int specKeyPressed, int mouseX, int mouseY);

//keyboard special keys input (release)
void onSpecialKeyRelease(int specKeyReleased, int mouseX, int mouseY);

//mouse input (movement)
void onMouseMovement(int mouseX, int mouseY);

//mouse input (click)
void onMouseClick(int button, int state, int x, int y);

//time changing loop
void onTimer(int);