/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#pragma once
#include <map>
#include <string>
#include <vector>
#include "pgr.h"
#include "sceneGraph.h"
#include "camera.h"
#include "globalvariables.h"


using namespace std;

//nodes
//--------------------------Cnode
Cnode::Cnode(Cnode* parent) : m_parent(parent), m_processedChildrenNO(0), m_applied(false) {}

Cnode* Cnode::up(vector<glm::mat4*>& collectedTransformations)
{
	m_processedChildrenNO = 0;
	m_applied = false;
	++m_parent->m_processedChildrenNO;
	return m_parent;
}

void Cnode::update() {}

//--------------------------CtransformationNode
CtransformationNode::CtransformationNode(Cnode* parent, glm::mat4 (*transformationFunction)()) : Cnode(parent), m_transformationFunction(transformationFunction)
{
	m_transformation = m_transformationFunction();
}

Cnode* CtransformationNode::up(vector<glm::mat4*> & collectedTransformations)
{
	m_processedChildrenNO = 0;
	m_applied = false;
	//currenttransformation *= glm::inverse(currenttransformation);
	collectedTransformations.pop_back();
	++m_parent->m_processedChildrenNO;
	return m_parent;
}

Cnode* CtransformationNode::down(vector<glm::mat4*>& collectedTransformations)
{
	if (m_applied == false) {
		//currenttransformation *= m_transformation;
		collectedTransformations.emplace_back(&m_transformation);
		m_applied = true;
	}
	return m_children[m_processedChildrenNO];
}

void CtransformationNode::update() 
{
	m_transformation = m_transformationFunction();
}

//--------------------------CmaterialNode
CmaterialNode::CmaterialNode(Cnode* parent, Tmodel* model) :Cnode(parent), m_model(model) {}

Cnode* CmaterialNode::down(vector<glm::mat4*>& collectedTransformations)
{
	if (m_applied == false) {
		if (m_model->shaderID == simpleShader.ID)
			simpleShaderMaterial();
		else if (m_model->shaderID == skyboxShader.ID)
			skyboxShaderMaterial();
		else if (m_model->shaderID == knifeShader.ID)
			knifeShaderMaterial();
		else if (m_model->shaderID == glassShader.ID)
			glassShaderMaterial();
		m_applied = true;
	}
	return m_children[m_processedChildrenNO];
}

void CmaterialNode::simpleShaderMaterial() 
{
	//which shader program to use
	glUseProgram(simpleShader.program);
	
	//material
	glUniform3fv(simpleShader.locationDiffuse, 1, glm::value_ptr(m_model->diffuse));
	glUniform3fv(simpleShader.locationAmbient, 1, glm::value_ptr(m_model->ambient));
	glUniform3fv(simpleShader.locationSpecular, 1, glm::value_ptr(m_model->specular));
	glUniform1f(simpleShader.locationShinines, m_model->shininess);
	//picking
	glUniform1f(simpleShader.locationShine, shine);

	//lights
	glUniform3fv(simpleShader.locationFlashlightPosition, 1, glm::value_ptr(camera.eye));
	glUniform3fv(simpleShader.locationFlashlightDirection, 1, glm::value_ptr(camera.currentCenterVector));

	//texture
	glUniform1i(simpleShader.locationUseTexture, 1);
	glUniform1i(simpleShader.locationTextureSampler, 0);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, m_model->texture);
}

void CmaterialNode::skyboxShaderMaterial() 
{
	glUseProgram(skyboxShader.program);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox->texture);
	glUniform1i(skyboxShader.locationTextureSampler, 0);
}

void CmaterialNode::knifeShaderMaterial()
{
	//which shader program to use
	glUseProgram(knifeShader.program);

	//material
	glUniform3fv(knifeShader.locationDiffuse, 1, glm::value_ptr(m_model->diffuse));
	glUniform3fv(knifeShader.locationAmbient, 1, glm::value_ptr(m_model->ambient));
	glUniform3fv(knifeShader.locationSpecular, 1, glm::value_ptr(m_model->specular));
	glUniform1f(knifeShader.locationShinines, m_model->shininess);
	//picking
	glUniform1f(knifeShader.locationOffset, knifeOffset);
	glUniform1f(knifeShader.locationShine, shine);

	//lights
	glUniform3fv(knifeShader.locationFlashlightPosition, 1, glm::value_ptr(camera.eye));
	glUniform3fv(knifeShader.locationFlashlightDirection, 1, glm::value_ptr(camera.currentCenterVector));

	//texture
	glUniform1i(knifeShader.locationUseTexture, 1);
	glUniform1i(knifeShader.locationTextureSampler, 0);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, m_model->texture);
}

void CmaterialNode::glassShaderMaterial()
{
	//which shader program to use
	glUseProgram(glassShader.program);

	//material
	glUniform3fv(glassShader.locationDiffuse, 1, glm::value_ptr(m_model->diffuse));
	glUniform3fv(glassShader.locationAmbient, 1, glm::value_ptr(m_model->ambient));
	glUniform3fv(glassShader.locationSpecular, 1, glm::value_ptr(m_model->specular));
	glUniform1f(glassShader.locationShinines, m_model->shininess);
	//picking
	if(glassOffset == true)
		glUniform1f(glassShader.locationOffset, gameTime/5);
	else
		glUniform1f(glassShader.locationOffset, -(gameTime / 5));
	glUniform1f(glassShader.locationShine, shine);

	//lights
	glUniform3fv(glassShader.locationFlashlightPosition, 1, glm::value_ptr(camera.eye));
	glUniform3fv(glassShader.locationFlashlightDirection, 1, glm::value_ptr(camera.currentCenterVector));

	//texture
	glUniform1i(glassShader.locationUseTexture, 1);
	glUniform1i(glassShader.locationTextureSampler, 0);
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, m_model->texture);
}

//--------------------------CmeshNode
CmeshNode::CmeshNode(Cnode* parent, Tmodel* model) : Cnode(parent), m_model(model) {}

Cnode* CmeshNode::down(vector<glm::mat4*>& collectedTransformations)
{
	if (m_model->shaderID == simpleShader.ID)
		drawSimpleShaderMesh(collectedTransformations);
	else if (m_model->shaderID == skyboxShader.ID)
		drawSkyboxShaderMesh();
	else if (m_model->shaderID == knifeShader.ID)
		drawKnifeShaderMesh(collectedTransformations);
	else if (m_model->shaderID == glassShader.ID)
		drawGlassShaderMesh(collectedTransformations);
	return this;
}

glm::mat4 CmeshNode::calculateModelMatrix(vector<glm::mat4*>& collectedTransformations) {
	glm::mat4 M = glm::mat4();
	for (auto it = collectedTransformations.rbegin(); it != collectedTransformations.rend(); ++it)
	{
		M *= **it;
	}
	return M;
}

void CmeshNode::drawSimpleShaderMesh(vector<glm::mat4*>& collectedTransformations) 
{	
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	//second parametr is id
	glStencilFunc(GL_ALWAYS, pickID, -1);
	++pickID;
	
	//camera matrices
	glm::mat4 M = calculateModelMatrix(collectedTransformations);
	glm::mat4 PVM = PV[0] * PV[1] * M;
	glUniformMatrix4fv(simpleShader.locationPVM, 1, GL_FALSE, glm::value_ptr(PVM));
	glUniformMatrix4fv(simpleShader.locationV, 1, GL_FALSE, glm::value_ptr(PV[1]));
	glUniformMatrix4fv(simpleShader.locationM, 1, GL_FALSE, glm::value_ptr(M));
	glm::mat4 N = M;
	//zero out last column (delete translation)
	N[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	N = glm::transpose(glm::inverse(N));
	glUniformMatrix4fv(simpleShader.locationN, 1, GL_FALSE, glm::value_ptr(N));

	// draw geometry
	glBindVertexArray(m_model->vao);
	glDrawElements(GL_TRIANGLES, m_model->trianglesNO * 3, GL_UNSIGNED_INT, 0);

	glDisable(GL_STENCIL_TEST);
}

void CmeshNode::drawSkyboxShaderMesh() {
	glm::mat4 viewRotation = PV[1];
	viewRotation[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	glm::mat4 inversePV = glm::inverse(PV[0] * viewRotation);

	glUniformMatrix4fv(skyboxShader.locationInversePV, 1, GL_FALSE, glm::value_ptr(inversePV));

	//draw two triangles as far plane
	glBindVertexArray(skybox->vao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, skybox->trianglesNO + 2);
}

void CmeshNode::drawKnifeShaderMesh(vector<glm::mat4*>& collectedTransformations) 
{
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	//second parametr is id
	glStencilFunc(GL_ALWAYS, 1, -1);

	//camera matrices
	glm::mat4 M = calculateModelMatrix(collectedTransformations);
	glm::mat4 PVM = PV[0] * PV[1] * M;
	glUniformMatrix4fv(knifeShader.locationPVM, 1, GL_FALSE, glm::value_ptr(PVM));
	glUniformMatrix4fv(knifeShader.locationV, 1, GL_FALSE, glm::value_ptr(PV[1]));
	glUniformMatrix4fv(knifeShader.locationM, 1, GL_FALSE, glm::value_ptr(M));
	glm::mat4 N = M;
	//zero out last column (delete translation)
	N[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	N = glm::transpose(glm::inverse(N));
	glUniformMatrix4fv(knifeShader.locationN, 1, GL_FALSE, glm::value_ptr(N));

	// draw geometry
	glBindVertexArray(m_model->vao);
	glDrawElements(GL_TRIANGLES, m_model->trianglesNO * 3, GL_UNSIGNED_INT, 0);

	glDisable(GL_STENCIL_TEST);
}

void CmeshNode::drawGlassShaderMesh(vector<glm::mat4*>& collectedTransformations)
{
	//camera matrices
	glm::mat4 M = calculateModelMatrix(collectedTransformations);
	glm::mat4 PVM = PV[0] * PV[1] * M;
	glUniformMatrix4fv(glassShader.locationPVM, 1, GL_FALSE, glm::value_ptr(PVM));
	glUniformMatrix4fv(glassShader.locationV, 1, GL_FALSE, glm::value_ptr(PV[1]));
	glUniformMatrix4fv(glassShader.locationM, 1, GL_FALSE, glm::value_ptr(M));
	glm::mat4 N = M;
	//zero out last column (delete translation)
	N[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	N = glm::transpose(glm::inverse(N));
	glUniformMatrix4fv(glassShader.locationN, 1, GL_FALSE, glm::value_ptr(N));

	// draw geometry
	glBindVertexArray(m_model->vao);
	glDrawElements(GL_TRIANGLES, m_model->trianglesNO * 3, GL_UNSIGNED_INT, 0);
}

//--------------------------CsceneGraph
CsceneGraph::CsceneGraph()
{
	m_nodes["root"] = new CtransformationNode(NULL, []() -> glm::mat4 {return glm::mat4();});
}

CsceneGraph::~CsceneGraph() {
	for (auto it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
		delete(it->second);
	}
}

void CsceneGraph::addTransformationNode(Cnode* parent, string name, glm::mat4 (*transformationFunction)(), bool animation) 
{
	m_nodes[name] = new CtransformationNode(parent, transformationFunction);
	parent->m_children.emplace_back(m_nodes[name]);
	if (animation)
		m_nodesToUpdate.emplace_back(m_nodes[name]);
}

void CsceneGraph::addMaterialNode(Cnode* parent, string name, Tmodel* model)
{
	m_nodes[name] = new CmaterialNode(parent, model);
	parent->m_children.emplace_back(m_nodes[name]);
}

void CsceneGraph::addMeshNode(Cnode* parent, string name, Tmodel* model)
{
	m_nodes[name] = new CmeshNode(parent, model);
	parent->m_children.emplace_back(m_nodes[name]);
}

void CsceneGraph::traverseGraph() 
{
	updateNodes();

	Cnode *node = m_nodes["root"];

	while (m_nodes["root"]->m_processedChildrenNO != m_nodes["root"]->m_children.size()) {
		//down
		while (node->m_processedChildrenNO != node->m_children.size()) {
			node = node->down(m_collectedTransformations);
		}
		//draw if no children
		if (node->m_children.size() == 0) {
			//(doesnt actually go down)
			node = node->down(m_collectedTransformations);
		}
		//up
		node = node->up(m_collectedTransformations);
	}
	m_nodes["root"]->m_processedChildrenNO = 0;

	pickID = 2;
}

void CsceneGraph::updateNodes() 
{
	for (Cnode *node : m_nodesToUpdate) 
	{
		node->update();
	}
}
