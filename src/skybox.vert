/*
* Luk� Marek *
* BI-PGR      *
* 2020-2021   *
*/

#version 140

uniform mat4 inversePV;
in vec2 screenCoordinations;
out vec3 vertexTextureCoordinates;

void main() 
{
  vec4 farPlaneCoordinates = vec4(screenCoordinations, 0.9999, 1.0);
  vec4 worldViewCoord = inversePV * farPlaneCoordinates;
  vertexTextureCoordinates = worldViewCoord.xyz / worldViewCoord.w;
  gl_Position = farPlaneCoordinates;
}